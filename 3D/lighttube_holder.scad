
dia = 50;
wall = 2.5;
height = 30;
profil_width =30.5;
profil_thick = 2.8;
join_tolerance = 0.5;

$fn=360;
difference()
{
    union()
    {
        cylinder(h=wall, d=135);
        translate([45,7.5,0])
        {
            cylinder(d=7,h=5);
        }
        translate([45,-7.5,0])
        {
            cylinder(d=7,h=5);
        }
    }
    translate([-30,0,-0.001])
    {
        cylinder(d=3.5,h=wall+0.002);
        cylinder(d1=0.001,d2=6,h=3);
    }
    translate([25,0,-0.001])
    {
        cylinder(d=3.5,h=wall+0.002);
        cylinder(d1=0.001,d2=6,h=3);
    }
    translate([45,7.5,-0.001])
    {
        cylinder(d1=6,d2=0.001,h=3);
        cylinder(d=3.5,h=10.002);
    }
    translate([45,-7.5,-0.001])
    {
        cylinder(d1=6,d2=0.001,h=3);
        cylinder(d=3.5,h=10.002);
    }
    translate([57,-28,-0.001])
    {
        cylinder(d=11,h=wall+0.002);
    }        
    translate([57,28,-0.001])
    {
        cylinder(d=11,h=wall+0.002);
    }
    translate([-57,-28,-0.001])
    {
        cylinder(d=11,h=wall+0.002);
    }
    translate([-57,28,-0.001])
    {
        cylinder(d=11,h=wall+0.002);
    }    
}
difference()
{
    union()
    {
        translate([0,0,35])
        {
            cube([profil_thick + wall * 2 ,42,70], center=true);
        }
        for(i=[-12,12])
        {
            for(j=[15, 60])
            {
                translate([profil_thick/2 + wall + 3,i,j])
                {
                    hull()
                    {
                        cube([6,12.5,10],center=true);
                        translate([-3,0,-3])
                        {
                            cube([0.001,12.5,13],center=true);
                        }
                    }
                }
            }
        }

    }
    for(i=[-12,12])
    {
        for(j=[15, 60])
        {    
            translate([0,i,j])
            {
                rotate([0,90,0])
                {
                    cylinder(d=4.5,h=10.001);
                }
            }
            translate([profil_thick/2 + wall + 1.5,i,j])
            {
                cube([3.5,7.5,10.002],center=true);
            }
        }
    }
    translate([0,0,37])
    {
        cube([profil_thick,profil_width,70],center=true);
    }
}
difference()
{
    for(i=[-1,1])
    {
        hull()
        {
            translate([0,21*i,70])
            {
                cube([profil_thick + wall * 2,3,0.001], center=true);
            }
            translate([0,21*i,0])
            {
                cube([125,3,0.001], center=true);
            }
        }
    }
    translate([-40,0,6])
    {
        cube([45,41,2], center=true);
    }
}


// Shell


difference()
{
    union()
    {
        cylinder(h=25, d=150);
        translate([0,0,25])
        {
            cylinder(h=53, d1=150,d2=50);
        }
    }
    translate([0,0,-0.001])
    {
        cylinder(h=25.002,d=140);
    }
    translate([0,0,-0.001])
    {
        difference()
        {
            cylinder(h=2, d=147);
            translate([0,0,-0.001])
            {
                cylinder(h=2.002,d=143);
            }
        }
    }
    translate([0,0,24.999])
    {
        cylinder(h=52.002, d1=140,d2=dia-2*wall);
    }
    translate([0,0,75.999])
    {
        cylinder(h=2.002,d=dia-2*wall);
    }
    // cube([100,100,100]);
    translate([72.5,0,19])
    {
        cube([10,12,10], center=true);
    }

    translate([-72.5,0,15])
    {
        rotate([0,90,0])
        {
            cylinder(d=2,h=10, center=true);
            for(i=[0:5])
            {
                rotate([0,0,60*i])
                {
                    translate([3,0,0])
                    {
                        cylinder(d=2,h=10, center=true);
                    }
                }
            }
        }
    }
}
translate([0,0,78])
{
    difference()
    {
        union()
        {
            cylinder(d=dia,h=height);
        }
        translate([0,0,-0.001])
            cylinder(d=dia-2*wall,h=height+2.002);
        translate([0,0,height-1.999])
            cylinder(d=dia-wall + join_tolerance,h=2);
    // cube([100,100,100]);
    }
}
for(i=[-1,1])
{
    for(j=[-1,1])
    {
        translate([57*i,28*j,0])
        {
            difference()
            {
                union()
                {
                    cylinder(d=9,h=10);
                    translate([0,0,2])
                    {
                        cylinder(d1=9,d2=11.5,h=8);
                    }
                    mirror([0.5-i/2,0,0])
                    {
                        rotate([0,0,(27.5*j)])
                        {
                            translate([2.5,-2,0])
                            {
                                cube([5,4,10]);
                            }
                        }
                    }
                }
                translate([0,0,-0.001])
                {
                    cylinder(d=4.5,h=10.002);
                }
                translate([0,0,6])
                {
                    cylinder(d=8*tan(30)*2, h=4.001, $fn=6);
                }
            }
        }
    }
}
/*
translate([57,28,-0.001])
{
    cylinder(d=11,h=wall+0.002);
}
translate([-57,-28,-0.001])
{
    cylinder(d=11,h=wall+0.002);
}
translate([-57,28,-0.001])
{
    cylinder(d=11,h=wall+0.002);
} 
*/


/*
difference()
{
    union()
    {
        cylinder(h=wall, d=135);
        translate([45,7.5,0])
        {
            cylinder(d=7,h=5);
        }
        translate([45,-7.5,0])
        {
            cylinder(d=7,h=5);
        }
    }
    translate([-30,0,-0.001])
    {
        cylinder(d=3.5,h=wall+0.002);
        cylinder(d1=0.001,d2=6,h=3);
    }
    translate([25,0,-0.001])
    {
        cylinder(d=3.5,h=wall+0.002);
        cylinder(d1=0.001,d2=6,h=3);
    }
    translate([45,7.5,-0.001])
    {
        cylinder(d1=6,d2=0.001,h=3);
        cylinder(d=3.5,h=10.002);
    }
    translate([45,-7.5,-0.001])
    {
        cylinder(d1=6,d2=0.001,h=3);
        cylinder(d=3.5,h=10.002);
    }
    translate([57,-28,-0.001])
    {
        cylinder(d=11,h=wall+0.002);
    }        
    translate([57,28,-0.001])
    {
        cylinder(d=11,h=wall+0.002);
    }
    translate([-57,-28,-0.001])
    {
        cylinder(d=11,h=wall+0.002);
    }
    translate([-57,28,-0.001])
    {
        cylinder(d=11,h=wall+0.002);
    }    
}
*/



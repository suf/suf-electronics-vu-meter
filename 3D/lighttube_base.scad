
dia = 50;
wall = 2.5;
height = 100;
profil_width =30.5;
profil_thick = 2.8;
join_tolerance = 0.5;

$fn=360;
difference()
{
    cylinder(h=wall, d=135);
    translate([-30,0,-0.001])
    {
        cylinder(d=3.5,h=wall+0.002);
        cylinder(d1=0.001,d2=6,h=3);
    }
    translate([25,0,-0.001])
    {
        cylinder(d=3.5,h=wall+0.002);
        cylinder(d1=0.001,d2=6,h=3);
    }    
}
difference()
{
    union()
    {
        translate([0,0,35])
        {
            cube([profil_thick + wall * 2 ,42,70], center=true);
        }
        for(i=[-12,12])
        {
            for(j=[15, 60])
            {
                translate([profil_thick/2 + wall + 3,i,j])
                {
                    hull()
                    {
                        cube([6,12.5,10],center=true);
                        translate([-3,0,-3])
                        {
                            cube([0.001,12.5,13],center=true);
                        }
                    }
                }
            }
        }
    }
    for(i=[-12,12])
    {
        for(j=[15, 60])
        {    
            translate([0,i,j])
            {
                rotate([0,90,0])
                {
                    cylinder(d=4.5,h=10.001);
                }
            }
            translate([profil_thick/2 + wall + 1.5,i,j])
            {
                cube([3.5,7.5,10.002],center=true);
            }
        }
    }
    translate([0,0,37])
    {
        cube([profil_thick,profil_width,70],center=true);
    }
}
difference()
{
    for(i=[-1,1])
    {
        hull()
        {
            translate([0,21*i,70])
            {
                cube([profil_thick + wall * 2,3,0.001], center=true);
            }
            translate([0,21*i,0])
            {
                cube([125,3,0.001], center=true);
            }
        }
    }
    translate([-40,0,6])
    {
        cube([45,41,2], center=true);
    }
}


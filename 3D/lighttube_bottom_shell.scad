
dia = 50;
wall = 2.5;
height = 30;
profil_width =30.5;
profil_thick = 2.8;
join_tolerance = 0.5;

$fn=360;

// Bottom cap
difference()
{
    union()
    {
        difference()
        {
            union()
            {
                cylinder(h=37.5, d=150);
                translate([0,0,37.5])
                {
                    difference()
                    {
                        cylinder(h=2, d=146.5);
                        translate([0,0,-0.001])
                        {
                            cylinder(h=2.002,d=143.5);
                        }
                    }
                }
                translate([65,-32.5,0])
                {
                    cube([30,65,25]);
                }
            }
            translate([0,0,2.5])
            {
                cylinder(h=35.002,d=140);
            }
            translate([64.999,-30,2.5])
            {
                cube([27.5,60,20]);
            }
            translate([92.498,-25,6])
            {
                cube([2.504,20,13]);
            }
            translate([92.498,3,6.5])
            {
                cube([2.504,23,12]);
            }
        }
        for(i=[-1,1])
        {
            for(j=[-1,1])
            {
                translate([57*i,28*j,0])
                {
                    cylinder(d=9,h=37.5);
                    translate([0,0,2.5])
                    {
                        cylinder(d1=16,d2=0.001,h=8);
                    }
                }
            }
        }
        for(i=[0:3])
        {
            rotate([0,0,45+i*90])
            {
                translate([65,-1.5,0])
                {
                    cube([6,3,37.5]);
                }
            }
        }
    }
    for(i=[-1,1])
    {
        for(j=[-1,1])
        {
            translate([57*i,28*j,-0.001])
            {
                cylinder(d=4.5,h=37.502);
                cylinder(d1=12,d2=0.001,h=6);
            }
        }
    }
}



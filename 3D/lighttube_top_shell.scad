
dia = 50;
wall = 2.5;
height = 30;
profil_width =30.5;
profil_thick = 2.8;
join_tolerance = 0.5;

$fn=360;

// Shell
difference()
{
    union()
    {
        cylinder(h=25, d=150);
        translate([0,0,25])
        {
            cylinder(h=53, d1=150,d2=50);
        }
    }
    translate([0,0,-0.001])
    {
        cylinder(h=25.002,d=140);
    }
    translate([0,0,-0.001])
    {
        difference()
        {
            cylinder(h=2, d=147);
            translate([0,0,-0.001])
            {
                cylinder(h=2.002,d=143);
            }
        }
    }
    translate([0,0,24.999])
    {
        cylinder(h=52.002, d1=140,d2=dia-2*wall);
    }
    translate([0,0,75.999])
    {
        cylinder(h=2.002,d=dia-2*wall);
    }
    cube([100,100,100]);
    translate([72.5,0,19])
    {
        cube([10,12,10], center=true);
    }

    translate([-72.5,0,12])
    {
        rotate([0,90,0])
        {
            cylinder(d=2,h=10, center=true);
            for(i=[0:5])
            {
                rotate([0,0,60*i])
                {
                    translate([3,0,0])
                    {
                        cylinder(d=2,h=10, center=true);
                    }
                }
            }
        }
    }
}
translate([0,0,78])
{
    difference()
    {
        union()
        {
            cylinder(d=dia,h=height);
        }
        translate([0,0,-0.001])
            cylinder(d=dia-2*wall,h=height+2.002);
        translate([0,0,height-1.999])
            cylinder(d=dia-wall + join_tolerance,h=2);
    cube([100,100,100]);
    }
}
for(i=[-1,1])
{
    for(j=[-1,1])
    {
        translate([57*i,28*j,0])
        {
            difference()
            {
                union()
                {
                    cylinder(d=9,h=10);
                    translate([0,0,2])
                    {
                        cylinder(d1=9,d2=11.5,h=8);
                    }
                    mirror([0.5-i/2,0,0])
                    {
                        rotate([0,0,(27.5*j)])
                        {
                            translate([2.5,-2,0])
                            {
                                cube([5,4,10]);
                            }
                        }
                    }
                }
                translate([0,0,-0.001])
                {
                    cylinder(d=4.5,h=10.002);
                }
                translate([0,0,6])
                {
                    cylinder(d=8*tan(30)*2, h=4.001, $fn=6);
                }
            }
        }
    }
}
// Bottom cap
translate([0,0,-45])
{
    difference()
    {
        union()
        {
            difference()
            {
                union()
                {
                    cylinder(h=37.5, d=150);
                    translate([0,0,37.5])
                    {
                        difference()
                        {
                            cylinder(h=2, d=146.5);
                            translate([0,0,-0.001])
                            {
                                cylinder(h=2.002,d=143.5);
                            }
                        }
                    }
                    translate([65,-32.5,0])
                    {
                        cube([30,65,25]);
                    }
                }
                translate([0,0,2.5])
                {
                    cylinder(h=35.002,d=140);
                }
                translate([64.999,-30,2.5])
                {
                    cube([27.5,60,20]);
                }
                translate([92.498,-25,6])
                {
                    cube([2.504,20,13]);
                }
                translate([92.498,3,6.5])
                {
                    cube([2.504,23,12]);
                }
            }
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([57*i,28*j,0])
                    {
                        cylinder(d=9,h=37.5);
                        translate([0,0,2.5])
                        {
                            cylinder(d1=16,d2=0.001,h=8);
                        }
                    }
                }
            }
            for(i=[0:3])
            {
                rotate([0,0,45+i*90])
                {
                    translate([65,-1.5,0])
                    {
                        cube([6,3,37.5]);
                    }
                }
            }
        }
        for(i=[-1,1])
        {
            for(j=[-1,1])
            {
                translate([57*i,28*j,-0.001])
                {
                    cylinder(d=4.5,h=37.502);
                    cylinder(d1=12,d2=0.001,h=6);
                }
            }
        }
    }
}



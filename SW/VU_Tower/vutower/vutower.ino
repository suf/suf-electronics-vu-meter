#include <FastLED.h>
#define NUM_LEDS 62
#define LED_LEVEL_0_MIN 39
#define LED_LEVEL_0_MAX 43
#define COLOR_NEGATIVE CRGB::Blue
#define COLOR_ZERO CRGB::Yellow
#define COLOR_POSITIVE CRGB::Red
#define LED_DATA_PIN 2

#define NOISE_FLOOR 30
#define VOL_MAX 500

CRGB leds[NUM_LEDS];


uint32_t aggregate = 0;
int Audio = 0;
int zero = 0;
int sample_count = 0;
int is_data_valid = 0;
uint32_t Loudness = 0;
uint32_t Display = 0;
uint32_t vol = 0;
void setup()
{
  FastLED.addLeds<NEOPIXEL, LED_DATA_PIN>(leds, NUM_LEDS);
}
void loop()
{
  Audio = analogRead(A0);
  sample_count++;
  aggregate += Audio;
  if(sample_count == 64)
  {
    zero = aggregate >> 6;
    aggregate >>= 1;
    sample_count = 32;
    is_data_valid = 1;
  }
  if(is_data_valid)
  {
      Loudness = 0;
      for(unsigned char i = 0; i < 32; i++)
      {
          Audio = analogRead(A0);
          Loudness += abs(Audio - zero);
      }
      Display = Loudness >> 3;
      Display = Display > NOISE_FLOOR ? Display - NOISE_FLOOR : 0;
      Display = (Display * NUM_LEDS) / VOL_MAX;
      for(int i = 0; i<NUM_LEDS-1; i++)
      {
        if(i < Display)
        {
          if(i <= (LED_LEVEL_0_MIN - 1))
          {
            leds[i] = COLOR_NEGATIVE;
          }
          if(i > (LED_LEVEL_0_MIN - 1) && i <= (LED_LEVEL_0_MAX - 1))
          {
            leds[i] = COLOR_ZERO;
          }
          if(i > (LED_LEVEL_0_MAX - 1))
          {
            leds[i] = COLOR_POSITIVE;
          }
        }
        else
        {
          leds[i] = CRGB::Black;
        }
      }
      FastLED.show();
      delay(50);
  }
}

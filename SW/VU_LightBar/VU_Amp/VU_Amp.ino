﻿/*
 Name:		VU_Amp.ino
 Created:	1/12/2023 9:41:09 PM
 Author:	GömöriZoltán
*/


// the setup function runs once when you press reset or power the board
void setup() {
	for(int i = 2; i < 11; i++)
	pinMode(i, OUTPUT);
}

// the loop function runs over and over again until power down or reset
void loop() {
	// Clear clock
	digitalWrite(4, LOW);
	digitalWrite(6, LOW);
	digitalWrite(8, LOW);
	digitalWrite(10, LOW);

	// Reset
	digitalWrite(2, HIGH);
	digitalWrite(2, LOW);
	digitalWrite(2, HIGH);

	delay(1000);
	// Set Data to clean state
	digitalWrite(3, HIGH);
	digitalWrite(5, HIGH);
	digitalWrite(7, HIGH);
	digitalWrite(9, HIGH);

	// Clear 2x3 block
	for (int i = 0; i < 7; i++)
	{
		digitalWrite(4, LOW);
		digitalWrite(4, HIGH);
	}
	for (int i = 0; i < 16; i++)
	{
		digitalWrite(6, LOW);
		digitalWrite(6, HIGH);
	}
	for (int i = 0; i < 16; i++)
	{
		digitalWrite(8, LOW);
		digitalWrite(8, HIGH);
	}
	for (int i = 0; i < 24; i++)
	{
		digitalWrite(10, LOW);
		digitalWrite(10, HIGH);
	}
	delay(1000);
}

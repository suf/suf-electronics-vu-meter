// 
// 
// 

#include "PinLightBar.h"

void setupPinLightBar()
{
	for (uint8_t i = 0; i < 10; i++)
	{
		pinMode(PinLightBarPins[i], OUTPUT);
	}
}
void setPinLightBar(signed char value)
{
	for (signed char i = 0; i < 10; i++)
	{
		digitalWrite(PinLightBarPins[i], i < value ? HIGH : LOW );
	}
}


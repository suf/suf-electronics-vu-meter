/*
 Name:		VU_LightBar.ino
 Created:	1/10/2023 7:30:20 PM
 Author:	SUF
*/

// the setup function runs once when you press reset or power the board
#include "arduino.h"
#include "PinLightBar.h"

uint32_t aggregate = 0;
int Audio = 0;
int zero = 0;
int sample_count = 0;
int is_data_valid = 0;
uint32_t Loudness = 0;
uint32_t Display = 0;

void setup() {
	setupPinLightBar();
}

// the loop function runs over and over again until power down or reset
void loop() {
  Audio = analogRead(A0);
  sample_count++;
  aggregate += Audio;
  if(sample_count == 64)
  {
    zero = aggregate >> 6;
    aggregate >>= 1;
    sample_count = 32;
    is_data_valid = 1;
  }
  if(is_data_valid)
  {
      Loudness = 0;
      for(unsigned char i = 0; i < 32; i++)
      {
          Audio = analogRead(A0);
          Loudness += abs(Audio - zero);
      }
      Display = Loudness >> 3;
      setPinLightBar(Display/6);
  }    
}

// PinLightBar.h

#ifndef _PINLIGHTBAR_h
#define _PINLIGHTBAR_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#ifdef __cplusplus
 extern "C" {
#endif

static const uint8_t PinLightBarPins[] = { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

extern void setupPinLightBar();
extern void setPinLightBar(signed char value);

#ifdef __cplusplus
}
#endif

#endif


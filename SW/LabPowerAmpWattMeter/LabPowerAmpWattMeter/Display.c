// 
// 
// 

#include "Arduino.h"
#include "Display.h"

void Display_Init()
{
	pinMode(DISPLAY_RESET, OUTPUT);
	digitalWrite(DISPLAY_RESET, HIGH);
	pinMode(U3_DATA, OUTPUT);
	digitalWrite(U3_DATA, HIGH);
	pinMode(U3_CLOCK, OUTPUT);
	digitalWrite(U3_CLOCK, LOW);
	pinMode(U45_DATA, OUTPUT);
	digitalWrite(U45_DATA, HIGH);
	pinMode(U45_CLOCK, OUTPUT);
	digitalWrite(U45_CLOCK, LOW);
	pinMode(U67_DATA, OUTPUT);
	digitalWrite(U67_DATA, HIGH);
	pinMode(U67_CLOCK, OUTPUT);
	digitalWrite(U67_CLOCK, LOW);
	pinMode(U89_DATA, OUTPUT);
	digitalWrite(U89_DATA, HIGH);
	pinMode(U89_CLOCK, OUTPUT);
	digitalWrite(U89_CLOCK, LOW);
	// Reset Display
	digitalWrite(DISPLAY_RESET, LOW);
	digitalWrite(DISPLAY_RESET, HIGH);
	U3 = 0;
	U45 = 0;
	U67 = 0;
	U89 = 0;
	DISPLAY_REWRITE = 0;
}

void Display_Clear()
{
	// Reset Display
	digitalWrite(DISPLAY_RESET, HIGH);
	digitalWrite(DISPLAY_RESET, LOW);
	digitalWrite(DISPLAY_RESET, HIGH);
	U3 = 0;
	U45 = 0;
	U67 = 0;
	U89 = 0;
	DISPLAY_REWRITE = 0x0F;
	Display_Update();
}

void Display_SendByte(uint16_t d_port, uint16_t c_port, uint8_t data)
{
	for (int i = 7; i >= 0; i--)
	{
		digitalWrite(d_port, ((data >> i) & 0x01) ^ 0x01);
		digitalWrite(c_port, HIGH);
		digitalWrite(c_port, LOW);
	}
}

void Display_SendWord(uint16_t d_port, uint16_t c_port, uint16_t data)
{
	for (int i = 15; i >= 0; i--)
	{
		digitalWrite(d_port, ((data >> i) & 0x0001) ^ 0x0001);
		digitalWrite(c_port, HIGH);
		digitalWrite(c_port, LOW);
	}
}

void Display_SetLED(uint32_t LED)
{
	uint8_t channel;
	uint16_t bitmask;
	channel = (LED >> 16) & 0x00FF;
	bitmask = LED & 0x0000FFFF;
	switch (channel)
	{
		case 0x00:
			U3 |= bitmask & 0x00FF;
			DISPLAY_REWRITE |= 0x01;
			break;
		case 0x01:
			U45 |= bitmask;
			DISPLAY_REWRITE |= 0x02;
			break;
		case 0x02:
			U67 |= bitmask;
			DISPLAY_REWRITE |= 0x04;
			break;
		case 0x03:
			U89 |= bitmask;
			DISPLAY_REWRITE |= 0x08;
			break;
	}
}

void Display_ClearLED(uint32_t LED)
{
	uint8_t channel;
	uint16_t bitmask;
	channel = (LED >> 16) & 0x00FF;
	bitmask = ~LED & 0x0000FFFF;
	switch (channel)
	{
	case 0x00:
		U3 &= bitmask & 0x00FF;
		DISPLAY_REWRITE |= 0x01;
		break;
	case 0x01:
		U45 &= bitmask;
		DISPLAY_REWRITE |= 0x02;
		break;
	case 0x02:
		U67 &= bitmask;
		DISPLAY_REWRITE |= 0x04;
		break;
	case 0x03:
		U89 &= bitmask;
		DISPLAY_REWRITE |= 0x08;
		break;
	}
}

void Display_LEDBar(uint8_t bar, uint8_t value)
{
	for (int i = 0; i < 12; i++)
	{
		if (value > i)
		{
			Display_SetLED(LedBar[bar][i]);
		}
		else
		{
			Display_ClearLED(LedBar[bar][i]);
		}
	}
}

void Display_Update()
{
	if (DISPLAY_REWRITE & 0x01) Display_SendByte(U3_DATA, U3_CLOCK, U3);
	if (DISPLAY_REWRITE & 0x02) Display_SendWord(U45_DATA, U45_CLOCK, U45);
	if (DISPLAY_REWRITE & 0x04) Display_SendWord(U67_DATA, U67_CLOCK, U67);
	if (DISPLAY_REWRITE & 0x08) Display_SendWord(U89_DATA, U89_CLOCK, U89);
	DISPLAY_REWRITE = 0;
}

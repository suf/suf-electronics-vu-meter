// VU.h

#ifndef _VU_h
#define _VU_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


// Analog PINs
#define PIN_VU_LEFT_V A0
#define PIN_VU_RIGHT_V A1
#define PIN_VU_LEFT_I A2
#define PIN_VU_RIGHT_I A3

#endif


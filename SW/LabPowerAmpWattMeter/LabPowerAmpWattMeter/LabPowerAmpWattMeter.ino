﻿/*
 Name:		LabPowerAmpWattMeter.ino
 Created:	10/6/2022 9:01:15 PM
 Author:	GömöriZoltán
*/

// the setup function runs once when you press reset or power the board
#include "Arduino.h"
#include "VU.h"
#include "Display.h"
void setup() {
	Display_Init();
	Display_Clear();
}

// the loop function runs over and over again until power down or reset
void loop() {
	/*
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 12; j++)
		{
			Display_SetLED(LedBar[i][j]);
			Display_Update();
			delay(150);
		}
	}
	Display_Clear();
	delay(150);
	*/
	for (int i = 0; i < 13; i++)
	{
		Display_LEDBar(0, i);
		Display_Update();
		delay(150);
	}
}

// Display.h

#ifndef _DISPLAY_h
#define _DISPLAY_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define LED01 0x00000001
#define LED02 0x00000002
#define LED03 0x00000004
#define LED04 0x00000008
#define LED05 0x00000010
#define LED06 0x00000020
#define LED07 0x00000040
#define LED08 0x00000080
#define LED09 0x00010001
#define LED10 0x00010002
#define LED11 0x00010004
#define LED12 0x00010008
#define LED13 0x00010010
#define LED14 0x00010020
#define LED15 0x00010040
#define LED16 0x00010080
#define LED17 0x00010100
#define LED18 0x00010200
#define LED19 0x00010400
#define LED20 0x00010800
#define LED21 0x00011000
#define LED22 0x00012000
#define LED23 0x00014000
#define LED24 0x00018000
#define LED25 0x00020001
#define LED26 0x00020002
#define LED27 0x00020004
#define LED28 0x00020008
#define LED29 0x00020010
#define LED30 0x00020020
#define LED31 0x00020040
#define LED32 0x00020080
#define LED33 0x00020100
#define LED34 0x00020200
#define LED35 0x00020400
#define LED36 0x00020800
#define LED37 0x00021000
#define LED38 0x00022000
#define LED39 0x00024000
#define LED40 0x00028000
#define LED41 0x00030001
#define LED42 0x00030002
#define LED43 0x00030004
#define LED44 0x00030008
#define LED45 0x00030010
#define LED46 0x00030020
#define LED47 0x00030040
#define LED48 0x00030080
#define LED49 0x00030100
#define LED50 0x00030200
#define LED51 0x00030400
#define LED52 0x00030800
#define LED53 0x00031000
#define LED54 0x00032000
#define LED55 0x00034000
#define LED56 0x00038000

// Arduino PINs
#define DISPLAY_RESET 2
#define U3_DATA 3
#define U3_CLOCK 4
#define U45_DATA 5
#define U45_CLOCK 6
#define U67_DATA 7
#define U67_CLOCK 8
#define U89_DATA 9
#define U89_CLOCK 10

const uint32_t LedBar[4][12] = {
	{LED55,LED51,LED47,LED43,LED39,LED35,LED31,LED27,LED23,LED19,LED15,LED11},
	{LED56,LED52,LED48,LED44,LED40,LED36,LED32,LED28,LED24,LED20,LED16,LED12},
	{LED53,LED49,LED45,LED41,LED37,LED33,LED29,LED25,LED21,LED17,LED13,LED09},
	{LED54,LED50,LED46,LED42,LED38,LED34,LED30,LED26,LED22,LED18,LED14,LED10}
};

uint8_t U3;
uint16_t U45;
uint16_t U67;
uint16_t U89;

uint8_t DISPLAY_REWRITE;

void Display_Init();
void Display_Clear();
void Display_SendByte(uint16_t d_port, uint16_t c_port, uint8_t data);
void Display_SendWord(uint16_t d_port, uint16_t c_port, uint16_t data);
void Display_SetLED(uint32_t LED);
void Display_ClearLED(uint32_t LED);
void Display_LEDBar(uint8_t bar, uint8_t value);
void Display_Update();

#ifdef __cplusplus
}
#endif

#endif

